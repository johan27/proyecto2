import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistarAlumnoComponent } from './registar-alumno.component';

describe('RegistarAlumnoComponent', () => {
  let component: RegistarAlumnoComponent;
  let fixture: ComponentFixture<RegistarAlumnoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistarAlumnoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistarAlumnoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

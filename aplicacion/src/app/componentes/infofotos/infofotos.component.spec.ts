import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { INFOfotosComponent } from './infofotos.component';

describe('INFOfotosComponent', () => {
  let component: INFOfotosComponent;
  let fixture: ComponentFixture<INFOfotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ INFOfotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(INFOfotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

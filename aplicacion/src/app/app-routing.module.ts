import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Route } from "@angular/compiler/src/core";

import { RouterModule, Routes } from "@angular/router";
import { MaterialDemoComponent } from "./material/material-demo/material-demo.component";
import { HomeComponent } from "./home/home.component";
import { InfomacionComponent } from "./infomacion/infomacion.component";



import { RegistarAlumnoComponent } from "./registar-alumno/registar-alumno.component";
import { EditarAlumnoComponent } from "./editar-alumno/editar-alumno.component";

import { EliminarAlumnoComponent } from "./eliminar-alumno/eliminar-alumno.component";



import { ServiciosComponent } from "./servicios/servicios.component";


import { ContactenosComponent } from "./contactenos/contactenos.component";
import { RegistarUsuarioComponent } from "./registar-usuario/registar-usuario.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full"
  },

  {
    path: "material-demo",
    component: MaterialDemoComponent
  },
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: "info",
    component: InfomacionComponent
  },

    {
    path: "contac",
    component: ContactenosComponent
  },
    {
    path: "servicios",
    component: ServiciosComponent
  },

    {
    path: "registarAlumno",
    component: RegistarAlumnoComponent
  },


  {
    path: "editalumno",
    component: EditarAlumnoComponent
  },
   {
    path: "eliminaralumno",
    component: EliminarAlumnoComponent
  },
 {
    path: "registarusuario",
    component: RegistarUsuarioComponent
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {}

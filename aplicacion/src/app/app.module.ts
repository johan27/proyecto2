import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { HttpClientModule } from "@angular/common/http";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppComponent } from "./app.component";
import { MaterialModule } from "./material/material.module";
import { AppRoutingModule } from ".//app-routing.module";
import { HomeComponent } from "./home/home.component";
import { FormsModule }   from '@angular/forms';
import { NasaApiService } from "./shared/services/nasa-api.service";
import { RegistarUsuarioComponent } from './registar-usuario/registar-usuario.component';
import { GeneroComponent } from './componentes/genero/genero.component';
import { INFOfotosComponent } from './componentes/infofotos/infofotos.component';
import { InfomacionComponent } from './infomacion/infomacion.component';
import { EditarAlumnoComponent } from './editar-alumno/editar-alumno.component';
import { CedulasComponent } from './componentes/cedulas/cedulas.component';
import { EliminarAlumnoComponent } from './eliminar-alumno/eliminar-alumno.component';
import { RegistarAlumnoComponent } from './registar-alumno/registar-alumno.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { ContactenosComponent } from './contactenos/contactenos.component';
import { FotosComponent } from './componentes/fotos/fotos.component';
import { ContacteComponent } from './componentes/contacte/contacte.component';

@NgModule({
  declarations: [AppComponent, HomeComponent, RegistarUsuarioComponent, GeneroComponent, INFOfotosComponent, InfomacionComponent, EditarAlumnoComponent, CedulasComponent, EliminarAlumnoComponent, RegistarAlumnoComponent, ServiciosComponent, ContactenosComponent, FotosComponent, ContacteComponent],
  imports: [
    BrowserModule,
     FormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    AppRoutingModule,
    HttpClientModule,
    
  ],
  providers: [NasaApiService],
  bootstrap: [AppComponent]
})
export class AppModule {}
